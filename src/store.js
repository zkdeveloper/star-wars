import rootReducer from "./redux/reducers/root";
import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import StarWars from "./redux/sagas/";
import { save, load } from "redux-localstorage-simple";

const sagaMiddleware = createSagaMiddleware();

const createStoreWithMiddleware = applyMiddleware(
  save({ states: ["favourites.favourites"] })
)(createStore);

const store = createStoreWithMiddleware(
  rootReducer,
  load({ states: ["favourites.favourites"] }),
  compose(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(StarWars);

export default store;
